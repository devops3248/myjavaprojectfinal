package com.example.newproject1;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Myfirstcontroller {

    @GetMapping("/greet")
    public String greetTheUser(){
        return "Hi Welcome";
    }

    @GetMapping("/")
    public String root(){
        return "This is Java Application";
    }
}
